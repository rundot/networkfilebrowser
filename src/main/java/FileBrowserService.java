import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rundot on 10.02.2017.
 */
public class FileBrowserService {

    private FileBrowser fileBrowser = new FileBrowser();

    public FileBrowserService() {
    }

    public FileBrowserService(String initPath) {
        fileBrowser.setCurrentPath(Paths.get(initPath));
    }

    //Returns normalized path to the current directory
    private String pwd() {
        return fileBrowser.getCurrentPath()
                .toAbsolutePath()
                .normalize()
                .toString();
    }

    //Lists content of the current directory with next flags
    //<DIR> directory
    //<F> file
    private List<String> ls() {
        return fileBrowser.list()
                .stream()
                .map(path -> (Files.isDirectory(path)?"<DIR>":"<F>") + path.getFileName().toString())
                .collect(Collectors.toList());
    }

    //Changes current directory and returns it's content
    private List<String> cd(String newPath) {
        Path path = fileBrowser.getCurrentPath().resolve(newPath);
        if (Files.exists(path) && Files.isDirectory(path))
            fileBrowser.setCurrentPath(path);
        return ls();
    }

    //Process given command and returns result of its execution
    public String process(Command command) {
        switch (command.getAction()) {
            case CD: return cd(command.getParam()).toString();
            case PWD: return pwd();
            case LS: return ls().toString();
            default: return "Unsupported command";
        }
    }

}
