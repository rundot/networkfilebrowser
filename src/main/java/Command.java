import java.io.Serializable;
import java.net.InetAddress;

/**
 * Created by rundot on 10.02.2017.
 */

//Represents Command which could be executed by class of this project
public class Command implements Serializable {

    private Action action;
    private String param;
    private InetAddress inetAddress;
    private int updPort;

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public void setInetAddress(InetAddress inetAddress) {
        this.inetAddress = inetAddress;
    }

    public int getUpdPort() {
        return updPort;
    }

    public void setUpdPort(int updPort) {
        this.updPort = updPort;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Command(Action action, String param) {
        this.action = action;
        this.param = param;
    }

    public Command(String strAction, String param) {
        Action action;
        try {
            action = Action.valueOf(strAction.toUpperCase());
        } catch (IllegalArgumentException e) {
            action = Action.NA;
        }
        this.action = action;
        this.param = param;
    }

    public Command(String command) {
        String strAction = command.indexOf(' ') > 0 ? command.substring(0, command.indexOf(' ')) : command;
        String param = command.indexOf(' ') > 0 ? command.substring(command.indexOf(' ') + 1).trim() : "";
        Action action;
        try {
            action = Action.valueOf(strAction.toUpperCase());
        } catch (IllegalArgumentException e) {
            action = Action.NA;
        }
        this.action = action;
        this.param = param;
    }

}
