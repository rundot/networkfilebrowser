import java.io.*;
import java.net.*;

/**
 * Created by rundot on 10.02.2017.
 */

    //Main class for the project
    //Client should listen to given UDP port
    //Client should choose its own TCP port
    //Client should accept connections on its TCP port
    //Client should auth itself by request from UDP port
public class NetworkFileBrowser {

    private static final int UDP_PORT = 12345;
    private static String inetAddr;
    private int tcpPort = 0;
    private DatagramSocket udpSocket;
    private ServerSocket tcpSocket;
    private Socket outConnection = null;

    //Accepts client connections and creates new thread for each of them
    private class TCPSocketListener extends Thread {
        public TCPSocketListener() {
            start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Socket clientSocket = tcpSocket.accept();
                    System.out.printf("Incoming connection from %s:%d accepted on port %d%n", clientSocket.getInetAddress(), clientSocket.getPort(), clientSocket.getLocalPort());
                    new ClientHandler(clientSocket);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class ClientHandler extends Thread {

        private Client client;

        public ClientHandler(Socket socket) {
            client = new Client(socket);
            start();
        }

        @Override
        public void run() {
            while (true) {
                Command command = SocketHelper.tcpReadCommand(client.getSocket());
                System.out.printf("Incoming command %s [%s] received from %s:%d%n", command.getAction(), command.getParam(), client.getSocket().getInetAddress(), client.getSocket().getPort());
                switch (command.getAction()) {
                    case CD:
                    case LS:
                    case PWD: {
                        Command answer = new Command(Action.SHOW, client.getService().process(command));
                        System.out.printf("Sending command %s [%s] to %s:%d%n", answer.getAction(), answer.getParam(), client.getSocket().getInetAddress(), client.getSocket().getPort());
                        SocketHelper.tcpWriteCommand(client.getSocket(), answer);
                        break;
                    }
                    case SHOW: {
                        System.out.println(command.getParam());
                        break;
                    }
                }
            }
        }
    }


    private class OutConnectionListener extends Thread {

        private Socket socket;

        public OutConnectionListener(Socket socket) {
            this.socket = socket;
            start();
        }

        @Override
        public void run() {
            while (true) {
                Command command = SocketHelper.tcpReadCommand(socket);
                if (command.getAction() == Action.SHOW) System.out.println(command.getParam());
            }
        }
    }

    //Thread for receiving commands from console
    private class ConsoleListener extends Thread {

        private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        public ConsoleListener() {
            start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Command command = new Command(reader.readLine());
                    switch (command.getAction()) {
                        case BROADCAST: {
                            SocketHelper.udpWriteCommand(udpSocket, command, InetAddress.getByName("255.255.255.255"), UDP_PORT);
                            break;
                        }
                        case CONNECT: {
                            if (outConnection != null && !outConnection.isClosed()) {
                                System.out.printf("Already connected to %s:%d%n", outConnection.getInetAddress(), outConnection.getPort());
                                break;
                            }
                            System.out.printf("Connecting to %s...%n", command.getParam());
                            String[] params = command.getParam().split(":");
                            outConnection = new Socket(InetAddress.getByName(params[0]), Integer.parseInt(params[1]));
                            new OutConnectionListener(outConnection);
                            SocketHelper.tcpWriteCommand(outConnection, new Command(Action.LS, ""));
                            System.out.println("Connected");
                            break;
                        }
                        case DISCONNECT: {
                            if (outConnection == null || outConnection.isClosed()) {
                                System.out.printf("You are not connected to any client%n");
                                break;
                            }
                            System.out.println("Disconnecting...");
                            outConnection.close();
                            System.out.println("Disconnected");
                            break;
                        }
                        case PWD:
                        case LS:
                        case CD: {
                            if (outConnection == null || outConnection.isClosed()) {
                                System.out.printf("You are not connected to any client%n");
                                break;
                            }
                            System.out.printf("Sending command %s [%s] to %s:%d%n", command.getAction(), command.getParam(), outConnection.getInetAddress(), outConnection.getPort());
                            SocketHelper.tcpWriteCommand(outConnection, command);
                            break;
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //Listens to UDP socket and send answer contains local IP and tcpPort;
    private class UDPSocketListener extends Thread {
        public UDPSocketListener() {
            start();
        }

        @Override
        public void run() {
            while (true) {
                    Command command = SocketHelper.udpReadCommand(udpSocket);
                    switch (command.getAction()) {
                        case BROADCAST: {
                            SocketHelper.udpWriteCommand(udpSocket, new Command(Action.SHOW, String.format("%s:%d", inetAddr, tcpPort)), command.getInetAddress(), command.getUpdPort());
                            break;
                        }
                        case SHOW: {
                            System.out.println(command.getParam());
                            break;
                        }
                    }
            }
        }
    }

    public NetworkFileBrowser() {
        try {
            inetAddr = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        while (tcpPort == 0) {
            try {
                tcpPort = (int) (Math.random() * 65535);
                tcpSocket = new ServerSocket(tcpPort);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            udpSocket = new DatagramSocket(UDP_PORT);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        new UDPSocketListener();
        new TCPSocketListener();
        new ConsoleListener();
    }

    public static void main(String[] args) {
        new NetworkFileBrowser();
    }

}

