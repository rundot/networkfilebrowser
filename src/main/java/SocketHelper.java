import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class SocketHelper {

    public static Command tcpReadCommand(Socket tcpSocket) {
        Command command = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(tcpSocket.getInputStream());
             command = (Command)ois.readObject();
//             ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return command;
    }

    public static void tcpWriteCommand(Socket tcpSocket, Command command) {
        try {
            ObjectOutputStream ous = new ObjectOutputStream(tcpSocket.getOutputStream());
            ous.writeObject(command);
//            ous.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Command udpReadCommand(DatagramSocket udpSocket) {
        byte[] buffer = new byte[256];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        try {
            udpSocket.receive(packet);
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(packet.getData()));
            Command command = (Command)ois.readObject();
            command.setUpdPort(packet.getPort());
            command.setInetAddress(packet.getAddress());
            ois.close();
            return command;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void udpWriteCommand(DatagramSocket udpSocket, Command command, InetAddress dest, int udpPort) {
        try (
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bout);) {
            oos.writeObject(command);
            byte[] buffer = bout.toByteArray();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, dest, udpPort);
            udpSocket.send(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
