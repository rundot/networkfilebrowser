import java.io.Serializable;

/**
 * Created by rundot on 10.02.2017.
 */
public enum Action implements Serializable {

    CD,
    LS,
    PWD,
    BROADCAST,
    CONNECT,
    DISCONNECT,
    NA,
    SHOW
}
