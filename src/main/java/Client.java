import java.net.Socket;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class Client {

    private Socket socket;
    private FileBrowserService service = new FileBrowserService("C:\\");

    public Client(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public FileBrowserService getService() {
        return service;
    }

    public void setService(FileBrowserService service) {
        this.service = service;
    }
}
