import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rundot on 10.02.2017.
 */
public class FileBrowser {

    private Path currentPath;

    public FileBrowser() {
        currentPath = Paths.get(".");
    }

    public FileBrowser(Path currentPath) {
        this.currentPath = currentPath;
    }

    public List<Path> list() {
        List<Path> pathList = new ArrayList<>();
        try {
            Files.newDirectoryStream(currentPath).forEach(path -> pathList.add(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pathList;
    }

    public Path getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(Path currentPath) {
        this.currentPath = currentPath;
    }
}
